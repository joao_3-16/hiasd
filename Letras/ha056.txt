As M�os do Senhor 

1.Eu vi essas m�os de meu Mestre, 
  Que p�o deram � multid�o, 
  Chamando gentis criancinhas 
  E aben�oando-as ent�o.

CORO: Oh! belas m�os, as m�os do Senhor;
      Crucificadas por ti!
      M�os que serviram em santo labor;
      M�os que inda velam por ti!

2. Aos pobres e fracos erguiam, 
   Curavam qualquer sofredor; 
   Serviam choupanas humildes, 
   Exemplificavam o amor.

3. Sim, hoje a caminho � cidade, 
   Da qual � Deus Pai construtor, 
   Est�o essas m�os a indicar-nos, 
   E estendem-se a ti, pecador.
