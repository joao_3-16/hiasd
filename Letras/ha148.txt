Cristo Vem 


1. Cristo vem com grande majestade
   Sua obra terminar.
   Oh, louvai o Rei da eternidade
   Que vir� aqui reinar.

CORO: Rei da paz, sim, vem reinar
      E vem levar-nos para o lar.
      Vem, Senhor Jesus, reinar.

2. J� se ouve o clarinar nos ares
   Da trombeta angelical,
   Que anuncia em todos os lugares
   O chamado divinal.
