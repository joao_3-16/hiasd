Por Belezas Naturais 
 1.
Por be lezas  natu rais,  pelo azul do  lindo  c�u,  
Por en cantos  imor tais,  � Senhor, ao  trono  Teu 
Se erguer�, e  com fer vor,  Nossa voz em  Teu lou vor. 


 2.
Por a migos  e ir m�os,  pela luz do  puro a mor,  
Por po derem  nossas  m�os  trabalhar em  Teu fa vor,  
Se erguer�, tam b�m, Se nhor,  Nossa voz em  Teu lou vor. 


 3.
Por Je sus, o  Salva dor,  que por n�s mor reu na  cruz, 
Pelo Es p�ri to de a mor  que � verdade  nos con duz,  
Se erguer�, tam b�m, Se nhor,  Nossa voz em  Teu lou vor. 
