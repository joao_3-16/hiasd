Onde Quer Que Seja 

		
1. Onde quer que seja com Jesus irei;
   Ele � meu bendito Salvador e Rei.
   Seja para a guerra, para batalhar,
   Seja para os campos, para semear.

CORO: Onde quer, onde quer que Jesus mandar
      Sempre junto dEle eu irei andar.

2. "Sempre seguirei a Cristo meu Senhor",
   Diz o cora��o que sente o Seu amor.
   Perto dEle sempre bem seguro vou
   Onde quer que seja mui contente estou.

3. Seja, pois, pra onde for que me levar,
   Eu encontrarei ali meu doce lar.
   Onde quer que seja sempre cantarei:
   "Tu, Senhor, comigo est�s; n�o temerei."
