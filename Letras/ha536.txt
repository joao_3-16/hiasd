Por um Pecador Qual Eu 


1. Por um pecador qual eu, 
   Sobre a cruz Jesus morreu; 
   Quis Seu sangue ali verter 
   Para eu poder viver;
   Sou eternamente Seu;
   DEle sou e Ele � meu.

2. Oh! que ilimitado amor 
   Revelou o Redentor!
   N�o � t�o profundo o mar, 
   Nem t�o alto o c�u sem-par!  
   Esse amor me procurou, 
   Despertou-me e me salvou.

3. Sei que pecador eu sou,
   Mas Jesus j� me salvou.
   Seu perd�o eu vou buscar, 
   Meus pecados lan�a ao mar, 
   Em Seus bra�os me sust�m, 
   E me guia at� o al�m.
