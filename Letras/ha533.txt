Cristo Salva 


1. �, vinde, v�s aflitos, j� 
   Ao trono do Senhor; 
   Abrigo Cristo vos dar�, 
   Ref�gio em Seu amor.

CORO: Cristo salva, Cristo salva,
      Salva o pecador!
      Cristo salva, Cristo salva,
      Por Seu grande amor!

2. Seu sangue derramou por n�s, 
   E assim nos veio abrir 
   Estrada reta que conduz
   Ao C�u, o bom porvir.

3. Jesus � vida, paz e luz,
   Do mundo o Redentor;
   E tudo, enfim, Jesus concede 
   Ao pobre pecador.

4. �, vinde, pois, �, vinde j�
   A Cristo vos unir!
   Perd�o Jesus conceder�
   A quem a luz seguir.
