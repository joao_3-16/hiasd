Can��o da Vida 

Pai querido obrigado pelos planos,
Grandes sonhos especiais pra mim.
Tu me guias nos caminhos desta vida.
Ao Teu lado nada faltar�.

Eu Te amo mais que tudo.
Quando forte, quando fraco,
Ao meu lado est�s.
Agrade�o Pai querido,
Por ligar a nossa vida 
Em num s� cora��o.

Eu Te amo mais que tudo.
Quando fraco, Tua for�a 
Nunca falhar�.
Por Tua gra�a, Pai, imploro 
Transformar a minha vida
Em uma can��o.
