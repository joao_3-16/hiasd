A Lei do Meu Senhor 


1. S�bia, justa, santa e pura
   � a lei do meu Senhor,
   Que corrige a vida impura
   Do perdido pecador.
   Do Senhor o ensinamento 
   Sempre ali perfeito est�;
   � t�o rica em Seus preceitos
   E conselhos santos d�!

2. Do Senhor os bons conselhos, 
   Justos e benignos s�o;
   Neles vejo, quais espelhos, 
   Quanto � mau meu cora��o.
   Mais que o Sol resplandecente, 
   Os preceitos do Senhor 
   Iluminam nossa mente,
   Com divino resplendor.
