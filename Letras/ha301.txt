Crer e Observar 

1. Em Jesus confiar, Sua lei observar,
   Oh, que gozo, que b�n��o, que paz!  
   Satisfeitos, guardar tudo quanto ordenar, 
   Alegria perene nos traz!

CORO: Crer e observar
      Tudo quanto ordenar;
      O fiel obedece
      Ao que Cristo mandar.

2. O inimigo falaz e a cal�nia mordaz
   Cristo pode desprestigiar;
   Nem tristeza, nem dor, nem a intriga maior
   Poder�o ao fiel abalar.

3. Que alegria sem-par, que prazer singular
   Tem o crente zeloso e leal!
   A Jesus contemplar, com Jesus sempre andar,
   Que consolo constante e real!

4. Resolutos, Senhor, e com f�, zelo e ardor,
   Os Teus passos queremos seguir;
   Teus preceitos guardar, o Teu nome exaltar, 
   Sempre a Tua vontade cumprir.
