Triunfante Vem Jesus 


1. Triunfante, glorioso, com a corte angelical,
   Jesus Cristo volve ao mundo sobre nuvem triunfal!
   Aleluia! O Seu reino vem fundar!
   Aleluia! O Seu reino vem fundar!

2. Em poder e majestade, vamos ver Jesus voltar.
   Mas os �mpios, criminosos, sim, Jesus vir� julgar.
   Com espanto, a Jesus ver�o chegar!
   Com espanto, a Jesus ver�o chegar!

3. Honra e gl�ria, aleluia ao Senhor, supremo Rei!
   Coroado, exaltado, vem guiar a Sua grei.
   No teu reino, vem, Senhor Jesus, reinar!
   No teu reino, vem, Senhor Jesus, reinar!
