Brilhemos por Jesus 

1. Deus d� �s criancinhas,
   Por onde Ele as conduz,
   O encargo t�o glorioso:
   Brilhar por seus Jesus.
   Sejamos belos raios
   Da luz do Deus de amor,
   Pra dissipar as trevas
   Dispersas ao redor.

CORO: Pequenos raios somos
      Da verdadeira luz;
      Em todos os lugares,
      Brilhemos por Jesus.

2. As nuvens t�o escuras 
   Escondem nossa luz;
   A vida tem tristezas,
   Pesada � nossa cruz.
   Mas, como belos raios,
   Cumpramos a miss�o
   De dar sempre alegria 
   A cada cora��o.

3. Que grande privil�gio:
   Brilhar, sim, por Jesus!
   E desfazer as trevas
   Pra dar lugar � luz!
   Com pensamentos puros,
   Vivamos com amor;
   Sejamos raiozinhos
   Que brilhem sem temor.
