Ser� de Manh�? 


1. Ser� de manh�, no come�o do dia?
   Ser� quando a luz seu fulgor irradia,
   Que Cristo h� de vir com os anjos da gl�ria,
   Receber deste mundo os Seus?

CORO: Oh, Jesus, Salvador, Senhor!
      Quando vamos cantar:
      Cristo volta, aleluia, aleluia, am�m!
      Aleluia, am�m?!

2. Ser� de manh� ou ser� pela tarde?
   Tamb�m pode ser que as trevas da noite
   Se tornem na luz desse brilho de gl�ria,
   Quando Cristo os Seus receber.

3. Oh! gra�a sem fim, quando a morte vencida,
   E Cristo Jesus revestir-nos de vida,
   E formos ent�o habitar l� na gl�ria,
   Nesse lar que Jesus preparou!
