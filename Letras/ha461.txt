Deus Sempre me Ama 

1. Deus sempre me ama,
   Co'amor me chama,
   E assim me inflama
   Do mesmo amor;

CORO: Por isso canto 
      O amor divino;
      Ser� meu hino
      O amor de Deus.

2. Cativo estive,
   Mas gra�a obtive
   Do amor que vive
   E faz viver;

3. Enviou Seu filho,
   Prestou-me aux�lio,
   Mostrou-me o trilho
   Que ao C�u conduz;

4. Jesus buscou-me,
   Jesus salvou-me,
   Ele aceitou-me
   Com terno amor;
