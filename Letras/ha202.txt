Eis uma Fonte 


1. Eis que uma fonte aberta est�:
   O sangue de Jesus.
   Quem nela se lavar, � cruz,
   Sem manchas ficar�.
   Sem manchas ficar�,
   Sem manchas ficar�;
   Quem nela se lavar, � cruz,
   Sem manchas ficar�.

2. O sangue do Cordeiro tem
   Poder pra libertar
   O pecador que a Cristo vem,
   Ao Salvador sem-par.
   Ao Salvador sem-par,
   Ao Salvador sem-par;
   O pecador que a Cristo vem,
   Ao Salvador sem-par.

3. Desde que aceitei a salva��o,
   Por Cristo, meu Senhor,
   Tem sido a excelsa reden��o
   Meu tema e meu vigor.
   Meu tema e meu vigor,
   Meu tema e meu vigor;
   Tem sido a excelsa reden��o
   Meu tema e meu vigor.

4. E quando � eterna gl�ria eu for,
   Melhor eu cantarei;
   A Cristo, Rei e Salvador,
   Pra sempre exaltarei.
   Pra sempre exaltarei,
   Pra sempre exaltarei;
   A Cristo, Rei e Salvador
   Pra sempre exaltarei.
