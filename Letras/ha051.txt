Clara Noite 


1. Clara noite, feliz e calma;
   Doce paz j� envolve o c�u.
   Vibram vozes em nossa alma,
   Pois Jesus, nosso Rei, nasceu.
   Rompam as fontes da alegria,
   Pois j� nasceu o Salvador.
   Rompam as fontes da alegria,
   Pois j� nasceu o Salvador.

2. Cantam anjos no firmamento
   Um louvor ao Menino Dues;
   Brilha a estrela, por um momento,
   A indicar o Senhor dos C�us.
   Todos cantemos: "Deus conosco,
   Noite feliz, feliz Natal!"
   Todos cantemos: "Deus conosco,
   Noite feliz, feliz Natal!"
