Confia em Deus 


1. A vida tem tristezas mil;
   Nem tudo � um c�u de anil,
   Mas contra a dor, que � t�o sutil,
   H� um caminho s�:

CORO:	Confia em Deus, que Ele sempre te ouvir�.
	Confia em Deus, que Ele nunca falhar�.
	Confia em Deus, que a negra nuvem passar�.
	Oh, n�o duvides, mas confia em Deus.

2. Quando teu c�u escurecer
   E a s�s penares teu sofrer,
   Se queres for�as receber,
   H� um caminho s�:

3. Se tua f� provada for
   E te esqueceres do Senhor,
   Necessitando um Salvador,
   H� um caminho s�:
