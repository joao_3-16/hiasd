Amor Sem Igual 


1. Oh, n�o tens ouvido do amor sem igual, 
   Do amor que teu Deus tem por ti? 
   O amor que O levou a Seu Filho entregar, 
   Pra os salvos levar para Si.

CORO: Oh, cr�! Sim, cr�!
      Oh, cr�! Sim, cr�!
      A gra�a de Deus te chama dos C�us.
      Oh, cr� nesse amor sem igual!

2. N�o foram os grandes que Cristo chamou, 
   Nem justos veio Ele salvar, 
   Mas pobres e fracos, culpados e maus 
   Mandou pelos servos buscar.

3. 0 homem, por�m, n�o podia chegar 
   � santa presen�a de Deus, 
   Porque os pecados de seu cora��o 
   Vedavam-lhe a entrada nos C�us.

4. Mas pelo Seu sangue Jesus expiou
   A culpa dos crentes, na cruz;
   Tirando o pecado, o caminho mostrou,
   O qual para os C�us nos conduz.
