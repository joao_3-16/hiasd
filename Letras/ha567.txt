A Cidade de Luz 

1. No livro de Deus eu j� li de um lar,
   A cidade onde noite n�o h�.
   O sol e o luar l� n�o h�o de existir,
   A gl�ria de Deus � a luz;
   A gl�ria de Deus � a luz.

CORO: Oh, cidade de luz onde noite n�o h�!
      L� irei encontrar, eu bem sei,
      Meus queridos de aqui e tamb�m meu Jesus,
      Na cidade de gl�ria e de luz.

2. Na pra�a central, bela �rvore est�,
   Nela o fruto da vida se v�.
   E as folhas dar�o para os filhos do Rei,
   Sa�de que n�o findar�;
   Sa�de que n�o findar�.

3. Oh, doce porvir, de celeste fulgor,
   Quando os salvos na gl�ria h�o de entrar!
   Ao lado de Deus, neste lar t�o feliz,
   Irei para sempre morar;
   Irei para sempre morar.
