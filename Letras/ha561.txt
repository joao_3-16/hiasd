No Celeste Lar Glorioso 

1. No celeste lar glorioso
   Da Jerusal�m de Deus,
   Eu espero mui ansioso
   Encontrar queridos meus!
   L� hei de rever amigos,
   Que me amavam como irm�o,
   E que aguardam, no jazigo,
   A feliz ressurrei��o.

CORO: Oh, vem tu tamb�m comigo,
      � nova Jerusal�m!
      Pois a ti, � meu amigo,
      Quero l� rever tamb�m.

2. L� anseio ver amados
   Que inda aqui em erro est�o,
   Pois Jesus, seu Advogado, 
   Inda faz intercess�o.
   L� desejo ouvir louvores
   De teus l�bios meu irm�o,
   Quando ap�s terrestres dores,
   N�s cantarmos em Si�o.

3. Quando l� no lar divino
   Ao teu lado eu caminhar,
   Oh! Que gozo peregrino!
   Oh! Que gl�ria singular!
   Queres este gozo ansiado, 
   Esta gl�ria que seduz?
   Sermos todos estreitados
   Pelos bra�os de Jesus?
