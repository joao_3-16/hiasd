Vinde, Meninos 


1. Vinde meninos, vinde a Jesus;
   Ele ganhou-vos b�n��os na cruz.
   Os pequeninos Ele conduz;
   Oh, vinde ao Salvador!

CORO: Que alegria, sem pecado ou mal,
      Reunir-nos todos, afinal,
      Juntos na p�tria celestial,
      Perto do Salvador!

2. J�, sem demora, muito conv�m
   Ir caminhando � gl�ria de al�m.
   Cristo vos chama, quer vosso bem;
   Oh, vinde ao Salvador!

3. Que ama aos meninos, Cristo vos diz;
   Quer receber-vos nesse pa�s,
   Quer conceder-vos vida feliz;
   Oh, vinde ao Salvador!
