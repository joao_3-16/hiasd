Conta as B�n��os 

1. Se da vida as ondas procelosas s�o,
   Se com desalento julgas tudo v�o,
   Conta as muitas b�n��os, dize-as duma vez,
   H�s de ver, surpreso, quanto Deus j� fez.

CORO: Conta as b�n��os, conta quantas s�o,
      Recebidas da divina m�o;
      Uma a uma, dize-as de uma vez;
      H�s de ver, surpreso, quanto Deus j� fez.

2. Tens acaso m�goas, triste � teu lidar?
   � a cruz pesada que tens de levar?
   Conta as muitas b�n��os, n�o duvidar�s,
   E em can��o alegre os dias passar�s.

3. Quando vires outros com seu ouro e bens,
   Lembra que tesouros prometidos tens;
   Nunca os bens da Terra poder�o comprar
   A mans�o celeste em que tu vais morar.

4. Seja teu conflito fraco ou forte aqui,
   N�o te desanimes, cuida Deus de ti;
   Seu divino aux�lio, minorando o mal,
   Dar-te-� consolo sempre, at� o final.
