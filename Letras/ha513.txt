As �guas Batismais 


1. Nas claras �guas deste mar, 
   Os meus pecados deixarei, 
   Depois, ent�o, irei morar 
   Com o divino Rei.

CORO: Deixarei no mar,
      Minhas culpas deixarei, deixarei; 
      Deixarei, no mar,
      E no C�u irei morar.

2. Desejo agora consagrar
   A minha vida ao Salvador, 
   Deixando todo o meu pesar 
   Aos p�s do Redentor.

3. Aqui as culpas vou deixar, 
   E vida nova ent�o terei,
   E assim um dia irei morar 
   Com o divino Rei.
