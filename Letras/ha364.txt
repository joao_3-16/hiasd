Sombras 


1. Muita vez das tristes sombras n�s falamos,
   Ao seguirmos pela senda rumo ao lar;
   Mas as sombras, as mais densas, olvidamos, 
   Quando ao Sol, ent�o, volvemos nosso olhar.

CORO: Cristo � o Sol que resplandece em nossa estrada, 
      Sombras vemos ao as costas Lhe voltar, 
      Vamos pois olhar a Cristo na jornada, 
      E veremos toda sombra se afastar

2. Se o esp�rito sentires perturbado,
   � porque � luz as costas deste j�;
   Olha a Cristo que teu cora��o cansado, 
   Inundado por Sua doce luz ser�.

3. Vamos, pois, volver a Cristo a alma aberta, 
   3. Ele que � do mundo a radiante luz,
   E das sombras desta vida nos liberta;
   E, felizes, seguiremos a Jesus. 
