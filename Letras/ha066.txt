Na Senda do Calv�rio 


1. Seguia tristemente e s�, ningu�m a me observar;
   Curvado ao fardo andava eu e quis desesperar;
   Queixei-me ent�o a Cristo, do mundo a me atrair,
   E Sua voz suave pude ouvir:
   "Na senda do Calv�rio, Meus p�s tamb�m cansei,
   E sob a cruz pesada, sem for�as Me prostrei;
   Cansado peregrino, a noite chega ao fim;
   �, toma a cruz e segue junto a Mim."

2. Do esfor�o meu por Ti, Jesus, �s vezes me gabei:
   Qu�o grande sacrif�cio fiz, na senda estreita andei;
   Deixei fortuna e fama a fim de Te seguir;
   E Tua voz suave pude ouvir:
   "Deixei o trono e a gl�ria, deixei do C� a luz,
   E Minhas m�os pregaram sobre uma rude cruz;
   Com tua m�o na Minha, sigamos, pois, assim;
   �, toma a cruz e segue junto a Mim."
