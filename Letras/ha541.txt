De Jesus a Doce Voz 


1. De Jesus, a doce voz, 
   Ouvi, eu, pecador.  
   Aceitei, de cora��o, 
   Jesus, meu Salvador.

CORO: Meu pecado, sim,
      J� na cruz pagou,
      E por gra�a sem igual,
      Jesus me resgatou.

2. Retid�o em mim n�o h�, 
   Por gra�a salvo sou.
   A Jesus sou devedor,
   Pois j� me perdoou.

3. Por Jesus eu gozo paz,
   E tenho Seu favor.
   Nada aqui me faltar�,
   Se estou junto ao Senhor.
