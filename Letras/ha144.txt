O Romper da Alva 


1. 'St� perto o dia de Cristo vir,
   Eu ou�o proclamar.
   A alva j� n�o tardar�,
   Eis que vem muito pr�ximo o alvorar.

CORO: J� n�o pode a alva demorar.
      J� n�o pode a alva demorar.
      A noite est� a fugir,
      O dia a entreluzir;
      J� n�o pode a alva demorar.

2. Sinais preditos, da Lua e Sol,
   Em terra, c�u e mar,
   Proclamam o breve arrebol,
   Sim, da vinda de Cristo o aproximar.

3. � vinda a hora de despertar,
   Deixando o orgulho vil.
   Fazendo sempre a luz brilhar,
   Conduzindo os perdidos ao redil.
