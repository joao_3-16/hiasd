Louvores ao Meu Rei 

1. 0, vinde crentes e entoai louvores a Jesus, 
   Que para nossa salva��o foi morto numa cruz. 
   Seu sangue derramou, de tudo me lavou, 
   Mais alvo do que a neve me tornou.

CORO: O sangue de Jesus me lavou, me lavou!
      O sangue de Jesus me lavou, me lavou!
      Alegre cantarei louvores a meu Rei,
      A meu Senhor Jesus que me salvou!

2. Conosco vinde vos unir na guerra contra o mal,
   E com o nosso Salvador em marcha triunfal;
   A todos proclamar a gra�a e seu poder;
   Seu sangue derramou pra nos salvar.

3. 0 grande Autor da salva��o � Cristo, o Salvador,
   O Rei dos reis, o Redentor, Jesus, o bom Senhor;
   Pois tudo vencer�, vit�ria nos dar�,
   � gl�ria, salvos, nos conduzir�.
