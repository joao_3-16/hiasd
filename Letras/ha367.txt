Ao Passares Pelas �guas 

1. "Ao passares pelas �guas, serei contigo;
   Ao passares pelas �guas, serei contigo.
   Rios caudalosos a ti n�o podem submergir;
   Ao passares pelo fogo n�o, oh, n�o te queimar�s!  
   Ao passares pelas �guas, serei contigo;
   Ao passares pelas �guas, serei contigo."

2. "Ao passares pelas �guas, serei contigo;
   Ao passares pelas �guas, serei contigo.
   As duras provas a ti n�o podem derrotar;
   Se por Minha causa sofres, Tu me glorificar�s.  
   Ao passares pelas �guas, serei contigo;
   Ao passares pelas �guas, serei contigo."
