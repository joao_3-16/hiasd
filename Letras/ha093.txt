Precioso � Jesus Para Mim 

1. Precioso � Jesus, o meu Rei, Salvador ; 
   A Ele  somente  darei meu  louvor.  
   � fonte divina  de for�a  e vigor ;   
   Preci oso � Jesus para mim.  

Coro  Precioso � Jesus para mim,  
      Precioso � Jesus para mim.  
      � meu Redentor meu fiel Protetor;    
      Precioso � Jesus  para mim.  

2. Jesus bate � porta do meu cora��o,  
   E espera  paciente,  quer dar-me o  perd�o.  
   Que  Seu sacrificio  n�o seja  em v�o;    
   Precioso � Jesus para mim.  

3. Preciosas promessas eu tenho  do  Rei;  
   Com Ele  num lar  perenal  morarei;  
   Num  reino de  luz Sua  face verei;    
   Precioso � Jesus para mim.  
