N�s O Veremos 


1. N�s O veremos retornando em gl�ria;
   Contemplaremos o Rei Salvador.
   Com muitos anjos n�s O louvaremos,
   Cantando em coro, Hosana! Am�m!

2. N�s O veremos na manh� gloriosa;
   Contemplaremos o Seu resplendor.
   Entoaremos hinos de vit�ria,
   Cantando em coro, Hosana! Am�m!

3. N�s O veremos em radiante brilho;
   Nosso louvor al�aremos ao C�u
   Com santos anjos O exaltaremos,
   Cantando em coro, Hosana! Am�m!
   Hosana! Am�m!
