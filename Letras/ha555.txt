At� Ent�o 


1. Meu cora��o alegre hoje canta,
   Ao relembrar que as dores cessar�o;
   Vir� o fim a este mundo escuro;
   Meu lar ser� nas terras de Si�o.
 
CORO: Mas t� ent�o eu seguirei cantando,
      T� ent�o contente eu hei de estar,
      At� o dia em que a cidade eu veja,
      E ou�a a Deus chamar-me ao lar.

2. A morte a e dor n�o mais pavor nos trazem,
   Pois eu bem sei que em breve findar�o.
   De todo o mal que aqui nos faz trementes,
   Ao recordar os salvos sorrir�o.
