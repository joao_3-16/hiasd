Sou Peregrino e Forasteiro 

1. Sou peregrino e forasteiro,
   Uma noite aqui demoro e nada mais.  
   N�o me detenhas, pois que vou indo 
   Pra onde h� fontes sempre fluindo.

CORO: Sou peregrino e forasteiro,
      Uma noite aqui demoro e nada mais.

2. Oh! quanta gl�ria l� brilha sempre!
   L� est� meu anelante cora��o.
   Aqui no mundo escuro e triste
   Eu ando errante, e a dor existe.

3. L� na cidade pra onde eu sigo,
   Meu Senhor, sim, meu Senhor � sua luz.  
   L� n�o h� pranto, n�o h� tristeza,
   Em tudo h� gra�a, real beleza.

4. � terra triste, eu vou deixar-te,
   Mas um dia voltar�s � perfei��o.
   Por Cristo foste criada linda,
   E restaurada ser�s ainda.
