Oh! Bela Terra de Esplendor 


1. Pra terra aben�oada vou,
   Ansioso peregrino sou 
   Em busca do feliz lugar 
   No qual eu hei de descansar.

CORO: Oh, bela terra de esplendor,
      Querida heran�a do Senhor;
      Olhando, velo al�m do mar
      Que em breve eu hei de atravessar, 
      A linda praia perenal,
      Do eterno lar celestial.

2. Comigo vai o meu Senhor,
   Do mal me guarda com amor, 
   De paz transborda o cora��o, 
   E certo estou da salva��o.

3. Na vastid�o celestial 
   H� de soar canto imortal 
   Da triunfante multid�o 
   Cantando a grande reden��o.
