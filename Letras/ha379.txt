� Mestre, o Mar se Revolta 


1. � Mestre, o mar se revolta, as ondas nos d�o pavor!
   O c�u se reveste de trevas, n�o temos um salvador!
   N�o se Te d� que morramos?  Podes assim dormir,
   Se a cada momento nos vemos j� prestes a submergir?

CORO: "As ondas atendem ao Meu mandar: Sossegai!
      Quer seja este revolto mar,
      A ira dos homens, o g�nio do mal,
      Tais �guas n�o podem a nau tragar,
      Que leva o Senhor, Rei do c�u e mar,
      Pois todos ouvem o Meu mandar:
      Sossegai!  Sossegai!
      Convosco estou para vos salvar;
      Sim, sossegai."

2. Mestre, na minha tristeza estou quase a sucumbir.
   A dor que perturba minh'alma, eu pe�o-Te, vem banir!
   De ondas do mar que me encobrem, quem me far� sair?
   Pere�o sem Ti, � meu Mestre!  Vem logo, vem me acudir!

3. Mestre, chegou a bonan�a, em paz eis o c�u e o mar!
   O meu cora��o goza calma que n�o poder� findar.
   Fica comigo, � meu Mestre, dono da Terra e C�u, 
   E assim chegarei bem seguro ao porto, destino meu.
