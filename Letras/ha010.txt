Louvemos o Rei 


1.
Louvemos o 	Rei, glorioso Senhor		
Oh, 	vamos cantar de Seu infindo amor!	
Escudo seguro � Seu grande poder,	

E 	sempre Seus 	filhos es	t� a de	fen	der.	

2.
Falemos de 	Deus, da 	gra�a sem-	par, 		

Do 	grande Se	nhor do 	C�u, da Terra e 	mar,	

Que 	forma 	a chuva, 	o 	ven	to e a 	luz.	

A 	forte tor	menta o 	Seu poder 	tra	duz.	
3.
Cantemos do 	Seu cui	dado por 	n�s, 		

E 	demos ao 	Pai lou	vor em alta 	voz,	

Lou	vemos, 	louvemos 	ao 	bom 	Cri	ador,	

A	migo sin	cero, fi	el e Re	den	tor.	

