Cristo, D�-nos Tua Paz 


1. �, deixa Cristo, o grande Mestre, 
   Envolver-te com amor;
   Abre o cora��o e escuta a Sua voz.
   Muitos problemas que te trazem 
   A tristeza e a dor,
   N�o ter�o lugar se Cristo em ti morar.

CORO: Cristo, � Cristo, 
      D�-nos Tua paz! 
      Cristo, � Cristo, 
      D�-nos Tua paz!

2. �, canta agora a melodia 
   Que enternece o cora��o,
   Com louvor suave e doce ao Senhor.
   Sim, toda l�grima e tristeza 
   H�o de desaparecer;
   H�s de ter mais vida em Cristo, o Salvador.
