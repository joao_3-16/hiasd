Num Ber�o de Palha 


1. Num ber�o de palha dormia Jesus,
   0 meigo Menino, coberto de luz,
   Num rude pres�pio, � noite, em Bel�m,
   Enquanto as estrelas brilhavam al�m.

2. Acorda o Menino o gado a mugir,
   Mas Ele n�o chora, Se p�e a sorrir.
   Eu Te amo, Menino nascido em Bel�m;
   �, guia meus p�s no caminho do bem.

3. Bem perto de n�s permanece, � Senhor,
   Guardando-nos sempre com Teu santo amor;
   Contigo queremos pra sempre ficar;
   �, d�-nos entrada no Teu doce lar.
