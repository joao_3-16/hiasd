Eis Que as Estrelas V�m 

Eis que as estrelas v�m,
Do Sol fenece a luz!
O cora��o mergulha em paz do al�m
Que atrai e que seduz;
E nesta hora sobe ao cora��o
A voz de anseios mil,
C�lido almejar de reden��o,
Saudade atroz de um lar gentil.
J� negras sombras v�m,
E cai a escurid�o!
A embarca��o dos sonhos meus 
Tamb�m se faz ao mar;
Eu posso ver a praia al�m;
Irei em breve ali chegar.
Espero ver meu Mestre,
E afinal encontrarei meu lar.
