Seu Maravilhoso Olhar 

1. Vivi t�o longe do Senhor, 
   Assim eu quis andar, 
   At� que eu encontrei o amor 
   Em Seu bondoso olhar.

CORO: Seu maravilhoso olhar!
      Seu maravilhoso olhar!
      Transformou meu ser todo o meu viver
      Seu maravilhoso olhar!

2. Seu corpo vi na rude cruz, 
   Sofrendo ali por mim, 
   E ouvi a voz do meu Jesus:
   "Por ti morri assim.�

3. Em contri��o ent�o voltei
   � fonte deste amor.
   Perd�o e paz em Cristo achei;
   Perten�o ao Salvador!
