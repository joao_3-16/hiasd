Santo, Santo 


1. Santo, santo, Pai bondoso,
   Santo, santo, Deus poderoso,
   Entregamos nossos cora��es a Ti, em devo��o.
   Pai bondoso, Te adoramos.

2. Santo, santo, Cristo amado,
   Pela cruz n�s somos redimidos;
   Elevamos nossas mentes em constante adora��o.
   Cristo amado, Te adoramos.

3. Santo, santo, Santo Esp�rito,
   Enche de poder nossas vidas.
   Suplicamos Tua gra�a, Teu auxilio e dire��o.
   Santo esp�rito, Te adoramos.
