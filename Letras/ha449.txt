Perfeito Amor 

1. Pai, colocaste j� no ser humano,
   Teus sentimentos, Teu divino amor.
   Eis que, segundo Teu sagrado plano,
   Vem este par perante Ti, Senhor.

2. Bem como �s aves d�s seguro abrigo,
   Bem como aos ninhos d�s tamb�m calor,
   Vem dar aos noivos lar ameno e amigo,
   Paz, prote��o e o mais sincero amor.

3. Vem conceder-lhes luz em sua hist�ria,
   F� confian�a, muito amor tamb�m;
   E, quando Cristo vier em Sua gl�ria,
   �, d�-lhes Tu, um lar feliz no al�m.
