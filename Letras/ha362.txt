Cristo Ajudar� 
Ada Ruth Habershon (1861-1918)	Robert Harkness (l880-1961)	


 n�o temas, porque eu sou contigo; n�o te assombres, porque eu sou teu Deus; eu te fortale�o, e te ajudo, e te sustento com a destra da minha justi�a. Isa�as 41:10 	

Hin�rio Adventista #362 Sem Midi! 		
1. Vacilando minha f�,
   Cristo ajudar�.  
   Perseguido, sem merc�, 
   Cristo ajudar�.

CORO: Cristo ajudara,
      Sim, me ajudara!
      Seu amor por mim n�o muda;
      Sempre ajudara.

2. Crente in�til eu serei
   Se n�o me valer,
   Nem servi�o prestarei,
   Sem o Seu poder.

3. Com Seu sangue me comprou, 
   N�o me deixar�;
   Vida eterna me outorgou; 
   Sim, me ajudar�.
