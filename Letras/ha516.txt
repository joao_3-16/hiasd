Agora Posso Ver 


1. Agora enfim eu posso ver 
   Da fonte, ali, correr
   O sangue que o meu Jesus 
   Por mim verteu na cruz.

CORO: Oh, fonte, sim, de vida e amor,
      Que vem tirar o mal e a dor!
      Oh, gra�as mil ao bom Jesus
      Que a vida deu por mim na cruz!

2. Que nova luz, mudan�a e paz 
   No cora��o se faz!
   O antigo ser, corrupto e vil, 
   N�o mais se mostra hostil.

3. Andando estou � luz do C�u; 
   Perd�o se deu ao r�u!
   Justi�a Cristo me outorgou,
   E agora salvo estou.

4. Grandiosa gra�a revelou 
   Quem sangue derramou!
   Por toda a vida hei de cantar 
   A reden��o sem-par!
