Feliz Manh� 

	
1. J� cansados seguimos de noite a penar,
   Na jornada da vida crist�,
   Mas ser� para n�s t�o brilhante o raiar
   Da gloriosa e feliz manh�.

CORO: Findar�o as agruras da vida,
      Quando a n�voa do mal passar.
      H� de a noite fugir ante o vivo luzir,
      Quando o dia eternal raiar.

2. H� t�o grandes mist�rios na trilha a seguir,
   E pren�ncios de lutas e dor,
   Mas de n�s fugir�o quando virmos luzir
   Sua face em real fulgor.

3. Mesmo que nos pare�a o caminho sem fim,
   Tendo os olhos a lacrimejar,
   Recompensas gloriosas teremos enfim,
   No bendito e eterno lar.
