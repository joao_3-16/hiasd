Jesus � o Salvador 
		

1. Somente Cristo � Salvador,
   E de outro n�o sabemos.  
   Morreu por n�s em santo amor 
   E vida nEle temos.

CORO: Jesus, Jesus � o Salvador,
      E n�o h� outro mediador.
      Foi Ele quem por n�s morreu,
      E vivo est� por n�s no C�u.

2. Os homens santos, bons, fi�is, 
   N�s sempre respeitamos;
   Mas a Jesus, o Rei dos reis, 
   Humildes adoramos.

3. No santo sangue de Jesus 
   Confia todo crente.
   O que Ele fez na rude cruz
   � todo suficiente.
