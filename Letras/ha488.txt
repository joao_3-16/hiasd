Novo Canto H� em Meu Ser 


1. Foram mui pesados os meus fardos de afli��o,
   Mas supremo al�vio em Cristo achou meu cora��o!

CORO: Novo canto h� em meu ser,
      Des'que Cristo me salvou;
      � um canto de prazer,
      Pois Jesus me resgatou.
      Toda culpa e todo mal
      Em Seu sangue, assim, lavou;
      Que prazer, que paz,
      Tanto amor me traz!
      Novo canto h� em meu ser!

2. Que transforma��o em minha vida se operou,
   Quando Cristo achou-me e meus pecados perdoou!

3. Nunca cessarei de amar a Cristo, meu Senhor,
   Pela salva��o que me proveu em Seu amor.

4. Muito em breve um canto angelical irei ouvir,
   Sim, ao coro alegre eu hei de a minha voz unir
