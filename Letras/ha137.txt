Anunciai Pelas Montanhas 

CORO: Anunciai pelas montanhas,
      E proclamai por terra e mar;
      Anunciai pelas montanhas
      Que Cristo vai voltar.

1. Minha'lma, com Cristo,
   Jamais desanimou,
   Porque Ele ajuda e guia,
   E a senda me mostrou.

2. Meu Mestre divino,
   Conduz pela m�o;
   De p�o sacia a alma,
   E escuta a ora��o.

3. E n�s, que O amamos,
   Devemos proclamar
   Que Cristo veio ao mundo,
   E a todos quer salvar.
