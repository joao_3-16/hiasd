Mans�o Sobre o Monte 


1. Aguardo o dia de emo��o sem-par, 
   No qual terei um bel�ssimo lar, 
   Entre colinas e vales formosos, 
   Donde se avista a cidade eternal.

CORO: Terei uma casa, no alto do monte,
      Com flores lindas por todo o jardim;
      Aves cantando nas copas frondosas,
      Rejubilando, na vida sem fim.

2. Manh�s bem claras, sem nuvens escuras, 
   O Sol radiante, est� sempre a brilhar; 
   Animaizinhos, sem susto ou temores, 
   Entre crian�as a correr e a brincar.
