As Searas Maduras 

	
1. Contemplai as searas maduras, 
   Vede os gr�os tremulando no ar; 
   Eis a voz do Senhor com ternura 
   Os fi�is para a ceifa chamar.

CORO: Oh! quem, pois, dir� para o Mestre:
      "Eis-me aqui, eu vou, Senhor!"
      Oh! quem a ceifar se consagra
      Com prazer, for�a e f�, com amor?

2. Do semear j� o tempo � passado,
   E chegou a esta��o outonal;
   Eis de Deus o anelante chamado: 
   "Partilhai da colheita final!"

3. Com prazer, afinal, o ceifeiro 
   Juntamente com o semeador, 
   Receber vai o pr�mio do obreiro, 
   L� no C�u, no eternal lar de amor.
