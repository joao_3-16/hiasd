S� um Passo 

1. Mui terna e doce � do Mestre a voz, 
   Chamando-me com amor:
   "De bra�os abertos te espero, vem! 
   �, vem ao teu Redentor!"

CORO: Cristo me chama, me quer salvar;
      � s� um passo, � s� confiar!
      Todo o meu ser deponho no altar
      Aceita-me Tu, � Cristo!

2. "Tu tens muitas culpas e d�bil f�", 
   Sugere-me o tentador.
   "Eu tudo j� fiz", uma voz me diz, 
   "Confia em teu Salvador!"

3. 0 mundo � falso, perdido e mau; 
   Pequeno e fr�gil sou, 
   Mas Cristo com voz divinal me diz: 
   "Meu filho, Eu for�as dou!"
