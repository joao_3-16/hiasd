Preitos de Louvor 


1. Pai, Te rendemos preitos de louvor,
   Pois Tua palavra mostra Teu amor.
   Em findo o culto conduzir-nos-�s,
   E para sempre Tu nos guiar�s.

2. Vem conduzir-nos para o nosso lar,
   Pois n�s queremos s� contigo andar.
   Oh, purifica nosso cora��o;
   A cada intento d� Tua dire��o.

3. Ao protegeres nosso caminhar,
   Pai, quantas b�n��os Teu amor nos traz!
   Mas, quando a luta nossa terminar,
   � Deus, concede Tua eterna paz.
