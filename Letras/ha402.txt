Fala � Minh'Alma 

		
1. Fala � minha alma � Cristo, fala-lhe com amor!
   Segreda com ternura: "Eu sou Teu Salvador!"        
   Faze-me bem disposto para te obedecer
   Sempre louvar Teu nome dedicar-te o ser.

CORO: Faze-me ouvir bem manso, em suave murmurar;
      Na cruz verti Meu sangue, para te libertar;
      Fala-me cada dia, fala com terno amor;
      Segreda-me ao ouvido: tu tens um Salvador.

2. Fala aos Teus filhos sempre, d�-lhes a dire��o;
   Enche-os de grande gozo mesmo na prova��o;         
   Faze-os bem consagrados, prontos a trabalhar;
   Para do reino eterno a vinda abreviar.
 
3. Como na antig�idade mostravas Teu querer;
   Revela-me hoje e sempre qual seja meu dever;
   A Ti somente eu quero louvores entoar;
   Teu nome eternamente engrandecer e honrar.
