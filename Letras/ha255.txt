Oh! Eu N�o Sei, Senhor 

1. Oh! eu n�o sei, Senhor, porque Tu querer�s
   Que tanto eu sofra aqui pesares e afli��o.
   Oh! eu n�o sei, Senhor, porque Tu querer�s
   Que eu sofra perdas se outra foi minha ora��o!

CORO:	Mas isto eu sei: nunca eu Te deixarei,
	Tu sabes, Pai, melhor, na prova��o maior;
	E sei tamb�m: se eu confiar em Ti,
	Breve as trevas passam e oh! qu�o claro aqui!

2. Oh! eu n�o sei, Senhor, porque Tu querer�s,
   Que eu ande neste vale escuro e t�o hostil!
   Oh! eu n�o sei, Senhor, porque Tu querer�s
   Que venham nuvens negras no meu c�u de anil!

3. Oh! eu n�o sei, Senhor, porque Tu querer�s
   Que venham sobre mim t�o duras tenta��es!
   Oh! eu n�o sei, Senhor, porque Tu querer�s
   Que eu sofra assim, na vida, amargas prova��es!
