Eu Sei em Quem Tenho Crido 

1. N�o sei porque de Deus o amor
   A mim se revelou,
   Por qual raz�o o Salvador
   Pra Si me resgatou.

CORO:	Mas eu sei em quem tenho crido,
	E estou bem certo que � poderoso,
	Pra guardar o meu tesouro
	At� o fim chegar.

2. N�o sei o que de mal ou bem
   Aqui enfrentarei;
   Se maus ou �ureos dias v�m,
   Se lutas eu terei.

3. N�o sei tamb�m o dia em que
   Verei Jesus voltar;
   Se de manh�, se � tarde vem
   Pra me levar ao lar.
