De Ti Care�o, � Deus 

1. De Ti care�o, � Deus, 
   Habita, pois, em mim, 
   E vencedor serei
   Fiel at� o fim.

CORO: Jesus, meu Salvador, 
      S� comigo aqui, 
      Bendize-me agora;
      Eu creio em Ti.

2. De Ti care�o, � Deus,
   De Ti, meu Salvador.
   �, vem fazer de mim, 
   Humilde servidor.

3. Em Tua santa lei, 
   Senhor, vem-me instruir; 
   E Teu t�o grande amor, 
   Almejo ent�o sentir.

4. Meu grande Protetor
   �s Tu, na prova��o; 
   Concedes-me vigor
   E eterna salva��o.
