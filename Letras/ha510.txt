Meu Jesus Est� Chamando 


1. Meu Jesus est� chamando,
   Com amor me convidando, 
   Sua voz, solicitando:
   Vem a Mim e segue-Me.

CORO: Minha cruz eu tomo e sigo, 
      A Jesus eu sempre sigo; 
      Aonde for, a Ele eu sigo, 
      Seguirei a meu Jesus.

2. Se o caminho for custoso, 
   E o trabalho mui penoso, 
   Mesmo assim fico animoso, 
   Quando sigo a meu Jesus.

3. Cristo sempre vai adiante 
   Com a Sua luz brilhante; 
   Sempre avan�o triunfante, 
   Quando sigo a meu Jesus.

4. Pelo vale Cristo guia; 
   Quando fraco, me auxilia; 
   Minhas dores alivia; 
   Seguirei a meu Jesus.
