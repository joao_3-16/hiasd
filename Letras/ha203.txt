� Jesus, Habita em Mim 

1. Meu pecado regatado
   Foi na cruz, por Teu amor;
   E da morte, triste sorte,
   Me livraste, � Senhor.

CORO: Vem, inflama viva chama
      De fervor e f� sem fim!
      Eu Te adoro, sempre imploro:
      � Jesus, habita em mim.

2. Se hesitante, vacilante,
   Ou�o a voz do tentador,
   Tu me guias, me auxilias
   E me tornas vencedor.

3. Redimida, s� tem vida
   A minh'alma em Teu amor.
   Com apre�o, reconhe�o
   Quanto devo a Ti, Senhor!
