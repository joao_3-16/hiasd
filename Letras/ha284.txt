Tudo por Cristo 
	
1. Teu, � Senhor bendito,
   sempre desejo ser.
   Livre do mundo in�quo,
   Quero tamb�m viver

CORO: Quero viver por Cristo,
      Tudo Lhe dedicar;
      Tudo por Cristo, tudo, tudo
      Quero renunciar.

2. Quero servir a Cristo,
   Sempre ao seu lado estar;
   �til na paz, na luta,
   Por Ele trabalhar

3. Queres, pois, aceitar-me
   Tal como sou, Senhor?
   Venho entregar-me agora
   �s o meu Redentor.
