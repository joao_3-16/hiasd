Haja Paz na Terra 


Haja paz na terra, a come�ar em mim;
Haja paz na terra, a come�ar em mim.
Irm�os n�s todos somos,
Filhos do mesmo Deus.
Juntos pois caminhemos,
Na paz que vem dos C�us.
A come�ar em mim, prometo ao meu Senhor
Que a cada passo que eu der, e seja onde for,
A cada momento estarei vivendo em plena paz e amor.
Haja paz na terra, a come�ar em mim.
